## What is this?

This image built ontop of [debian](https://registry.hub.docker.com/u/library/debian/) with commonly used packages pre-installed.

The following packages are bundled:

  * `curl` and `wget`
  * `unzip`
  * `gosu` - for easy step down


![logo](http://upload.wikimedia.org/wikipedia/commons/thumb/4/4a/Debian-OpenLogo.svg/50px-Debian-OpenLogo.svg.png)
